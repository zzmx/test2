//
//  tcp_net_socket.hpp
//  httptest
//
//  Created by zzm on 2016/12/10.
//  Copyright © 2016年 zzm. All rights reserved.
//

#ifndef tcp_net_socket_hpp
#define tcp_net_socket_hpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>

extern int tcp_init(const char* ip, int port);
extern int tcp_accept(int sfd);
extern int tcp_connect(const char* ip, int port);
extern void signalhandler(void);

#endif /* tcp_net_socket_hpp */
