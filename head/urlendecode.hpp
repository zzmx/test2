//
//  urlendecode.hpp
//  httptest
//
//  Created by zzm on 2016/12/11.
//  Copyright © 2016年 zzm. All rights reserved.
//

#ifndef urlendecode_hpp
#define urlendecode_hpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
//std::string UrlDecode(const std::string& szToDecode);
//std::string unicodeTostring(std::string str);
int enc_unicode_to_utf8_one(unsigned long unic, unsigned char *pOutput,
                            int outSize);
#endif /* urlendecode_hpp */
