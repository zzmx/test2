//
//  baidu_translate.hpp
//  opencvtest
//
//  Created by zzm on 2016/12/21.
//  Copyright © 2016年 zzm. All rights reserved.
//

#ifndef baidu_translate_hpp
#define baidu_translate_hpp
#include <iostream>
#include <cstring>
#include <string.h>
std::string translate(std::string translatestr);

#endif /* baidu_translate_hpp */
