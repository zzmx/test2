//
//  tcp_net_socket.cpp
//  httptest
//
//  Created by zzm on 2016/12/10.
//  Copyright © 2016年 zzm. All rights reserved.
//

#include "../head/tcp_net_socket.hpp"

int tcp_init(const char* ip, int port)
{
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd == -1)
    {
        perror("socket error!");
        exit(-1);
    }
    struct sockaddr_in serveraddr;
    memset(&serveraddr, 0, sizeof(struct sockaddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(port);
    serveraddr.sin_addr.s_addr = inet_addr(ip);
    if(bind(sfd, (struct sockaddr*)&serveraddr, sizeof(struct sockaddr)) == -1){
        perror("bind error");
        close(sfd);
        exit(-1);
    }
    if(listen(sfd, 10) == -1){
        perror("listen error");
        close(sfd);
        exit(-1);
    }
    return sfd;
}

//int tcp_accept(int sfd)
//{
//    struct sockaddr_in clientaddr;
//    memset(&clientaddr, 0, sizeof(struct sockaddr));
//    int addrlen = sizeof(struct sockaddr);
//    int new_fd = accept(sfd, (struct sockaddr*)&clientaddr, &addrlen);
//    if(new_fd == -1){
//        perror("accept error");
//        close(sfd);
//        exit(-1);
//    }
//    printf("%s %d success connect...", inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));
//    return new_fd;
//}

int tcp_connect(const char* ip, int port)
{
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd == -1){
        perror("socket error");
        exit(-1);
    }
    struct sockaddr_in serveraddr;
    memset(&serveraddr, 0, sizeof(struct sockaddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(port);
    serveraddr.sin_addr.s_addr = inet_addr(ip);
    if(connect(sfd, (struct sockaddr*)&serveraddr, sizeof(struct sockaddr)) == -1){
        perror("connect error");
        close(sfd);
        exit(-1);
    }
    return sfd;
}
