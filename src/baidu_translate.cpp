//
//  baidu_translate.cpp
//  opencvtest
//
//  Created by zzm on 2016/12/21.
//  Copyright © 2016年 zzm. All rights reserved.
//

#include "../head/baidu_translate.hpp"
#include <curl/curl.h>
#include <curl/easy.h>
#include "../head/TL_MD5.hpp"
#include "../head/urlendecode.hpp"

using namespace std;
//FILE *fp;
string utf_8str = "";
string Unicodestr = "";
void write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
//    int written = fwrite(ptr, size, nmemb, (FILE *)fp);
    Unicodestr = (char*)ptr;
}

string translate(string translatestr)
{
    FILE *fp;
    int i;
    MD5_CTX md5;
    MD5Init(&md5);
    utf_8str = "";
    Unicodestr = "";
    string str = "2015063000000001" + translatestr + "143566028812345678";
    long int strnum = str.length();
    unsigned char *encrypt;//21232f297a57a5a743894a0e4a801fc3
    encrypt = new unsigned char[strnum];
    for(i = 0; i < strnum; i++)
        *(encrypt+i) = str[i];
    //str[i] = '\0';
    unsigned char decrypt[16];
    MD5Update(&md5,encrypt,strnum);
    MD5Final(&md5,decrypt);
    printf("\n加密前:%s\n加密后32位:",encrypt);
    
    CURL *curl;
    CURLcode res;
    
     if((fp=fopen("/root/test.txt","w"))==NULL)
     {
    curl_easy_cleanup(curl);
         perror("FILE ERROR");
         exit(1);
     }
    
    
    string url = "api.fanyi.baidu.com/api/trans/vip/translate?q="+translatestr+"&from=en&to=zh&appid=2015063000000001&salt=1435660288&sign=";
    char tmp[3] = {'\0'};
    for(i=0;i<16;i++)
    {
        printf("%02x",decrypt[i]);  //02x前需要加上 %
        sprintf(tmp, "%2.2x", decrypt[i]);
        url += tmp;
    }
    cout << url << endl;
    curl_global_init(CURL_GLOBAL_ALL);
    curl=curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    if(curl != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        cout << "write ok" << endl;
    }
    unsigned long pos = -1;
    string tempstr = "";
    if(Unicodestr != "")
    {
        cout << Unicodestr << endl;
        string dststr = "dst";
        pos = Unicodestr.find(dststr);
        cout << "pos:" << pos << endl;
        tempstr = Unicodestr.substr(pos+6,(Unicodestr.length()-(pos+6))-11);
        
    }
    const char* tempstrchs = tempstr.c_str();
    printf("%s\n", tempstrchs);
    unsigned char outstr[6];
    bool escape = false;
    long int len = strlen(tempstr.c_str());
    int intHex;
    char temp[5];
    memset(temp, 0, 5);
    for (int i = 0; i < len; i++)
    {
        char c = tempstrchs[i];
        switch (c)
        {
            case '\\':
            case '%':
                escape = true;
                break;
            case 'u':
            case 'U':
                if (escape)
                {
                    memcpy(tmp, tempstrchs+i+1, 4);
                    sscanf(tmp, "%x", &intHex); //把16进制字符转换为数字
                    enc_unicode_to_utf8_one(intHex, outstr, 1);
                    utf_8str = utf_8str + (char*)outstr;
                }
                break;
        }
    }

    return utf_8str;
}
