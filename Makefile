vpath %.cpp src
vpath %.hpp head
CC      = g++ 
CFLAGS  = -Wall -O
INCLUDEFLAGS = -Wall -Wconversion -O3 `pkg-config --cflags opencv`
LDFLAGS = `pkg-config --libs opencv` -lcurl
OBJS    = main.o baidu_translate.o tcp_net_socket.o TL_MD5.o urlendecode.o
TARGETS = main 

.PHONY:all 
all : $(TARGETS)

main : main.o $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

%.o : %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(INCLUDEFLAGS)

%.d : %.c
	@set -e; rm -f $@; $(CC) -MM $< $(INCLUDEFLAGS) > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

-include $(OBJS:.o=.d)

.PHONY:clean 
clean:
	rm -f $(TARGETS) *.o *.d *.d.*
